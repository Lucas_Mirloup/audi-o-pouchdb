async function getAllDocs(db) {
    return (await db.allDocs({include_docs: true}))['rows'].map(row => row.doc);
}

export async function getAuthor(db, author_id) {
    const allDocs = await getAllDocs(db);
    const author = allDocs.find(doc => doc['_id'] === author_id);
    if (author) {
        await parseAuthor(db, author);
    }
    return author;
}

async function parseAuthor(db, author){
    const allDocs = await getAllDocs(db);
    author.albums = allDocs.filter(doc => doc['type'] === 'album' && doc['author'] === author._id);
    for (const album of author.albums)
        await parseAlbum(db, album, author);
}

export async function getAlbum(db, album_id) {
    const allDocs = await getAllDocs(db);
    const album = allDocs.find(doc => doc['_id'] === album_id);
    if (album) {
        await parseAlbum(db, album);
    }
    return album;
}

export async function getAlbums(db) {
    const allDocs = await getAllDocs(db);
    const albums = allDocs.filter(doc => doc['type'] === 'album');
    for (const album of albums)
        await parseAlbum(db, album);
    return albums;
}

async function parseAlbum(db, album, author) {
    if (author)
        album.author = author
    else
        album.author = await getAuthor(db, album.author);
    if (!album.coverUrl)
        album.coverUrl = "https://alphafa.com/wp-content/uploads/2018/09/placeholder-square.jpg";
    const songs = [];
    for (const song_name of album.songs)
        songs.push({title: song_name, album: album, author: album.author});
    album.songs = songs;
}

export async function getSongs(db) {
    const albums = await getAlbums(db);
    const songs = [];
    for (const album of albums)
        songs.push(...album.songs);
    return songs;
}

export async function getAuthors(db) {
    const allDocs = await getAllDocs(db);
    const authors = allDocs.filter(doc => doc['type'] === 'author');
    for (const author of authors)
        await parseAuthor(db, author);
    return authors;
}

export function deSerializeAlbum(album) {
    return {
        type: "album", _id: album._id, _rev: album._rev, author: album.author._id, title: album.title,
        songs: album.songs.map(s => s['title']), coverUrl: album.coverUrl
    }
}

export function deSerializeAuthor(author) {
    return {type: "author", _id: author._id, _rev: author._rev, name: author.name};
}
